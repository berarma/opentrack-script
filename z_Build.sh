#! /bin/bash
# Package requirements (Arch Linux names):
# cmake, ninja, qt5-base, opencv, xplane-sdk-devel
#
# Package requirements (Debian Linux names):
# cmake cmake-curses-gui ninja-build qttools5-dev qtbase5-private-dev libprocps-dev libopencv-dev libevdev-dev gcc-multilib g++-multilib wine wine32 wine32-tools wine32-tools libqt5serialport5-dev qv4l2
#
# Original program by JT8D-17 https://github.com/JT8D-17/opentrack.git
#
# Adapted by Francisco T. (a.k.a. P_Vader) from jugandoenlinux.com
#

# Set installation prefix path
# otdir="$(dirname "$PWD")/opentrack-install"
otdir="$HOME/.opentrack-install"
desktop_dir="${XDG_DATA_HOME:-$HOME/.local/share}/applications"
otsrc="$PWD"
logfile="$otsrc/z_buildlog.txt"

xplane_sdk_dir=""
onnxruntime_dir=""
arucodir="$otsrc/0_libaruco"
arucolib=""

clear

# Function: Pause
function pause(){
   echo "Press any key to continue"
   read -p "$*"
}

function clean_buildlog(){
    if [ -f "$logfile" ]; then
        rm "$logfile"
    fi
    echo "Previous log file deleted"
}

function build_folder(){
    if [ ! -d "$otsrc/build" ]; then
        echo "Creating build folder"
        mkdir "$otsrc/build"
    fi
}

function clean_build(){
    if [ -d "$otsrc/build" ]; then
        echo "Deleting and recreating build folder"
        rm -rf "$otsrc/build"
#         mkdir "$otsrc/build"
    fi
}

function get_xplane_sdk(){
    echo "Downloading X-Plane SDK headers"
    cd "$otsrc/build"
    wget -c https://developer.x-plane.com/wp-content/plugins/code-sample-generation/sample_templates/XPSDK303.zip
    unzip XPSDK303.zip
    mv SDK/ xplane_sdk/

}

function get_onnxruntime(){
    echo "Downloading ONNXRuntime"
    cd "$otsrc/build"
    wget -c https://github.com/microsoft/onnxruntime/releases/download/v1.6.0/onnxruntime-linux-x64-1.6.0.tgz
    tar -xzf onnxruntime-linux-x64-1.6.0.tgz
    mv onnxruntime-linux-x64-1.6.0/ onnxruntime-src/
}

function build_aruco(){
    if [ ! -d "$arucodir" ]; then
        mkdir "$arucodir"
        git clone https://github.com/opentrack/aruco.git "$arucodir"
    else
        cd "$arucodir"
        git pull
    fi
    if [ ! -d "$arucodir/build" ]; then
        mkdir "$arucodir/build"
    else
        rm -rf "$arucodir/build"
        mkdir "$arucodir/build"
    fi
    cd "$arucodir/build"
    cmake ".."
    cmake --build .
    make
    cd "$otsrc/build"
}


clean_buildlog

for arg in "$@"; do
  case "$arg" in
    clean)
        echo "Cleaning build folder"
        clean_build ;;
  esac
done

build_folder 2>&1 | tee "$logfile"

while [ ! -z "$1" ]; do
  case "$1" in
    aruco)
         echo "Building with Aruco."
         build_aruco 2>&1 | tee "$logfile"
         arucolib="$arucodir/build/src/libaruco.a"
         ;;
    xplane)
         echo "Building with X-plane."
         get_xplane_sdk 2>&1 | tee "$logfile"
         xplane_sdk_dir="xplane_sdk"
         ;;
     onnx)
         echo "Building with ONNXRuntime."
         get_onnxruntime 2>&1 | tee "$logfile"
         onnxruntime_dir="onnxruntime-src"
         ;;
  esac
shift
done

echo "Building Opentrack"
cd "$otsrc/build"

cmake .. \
    -GNinja \
    -DCMAKE_BUILD_TYPE=Debug \
    -DONNXRuntime_DIR="$onnxruntime_dir" \
    -DSDK_XPLANE="$xplane_sdk_dir" \
    -DSDK_ARUCO_LIBPATH="$arucolib" \
    -DSDK_WINE=ON \
    -DCMAKE_INSTALL_PREFIX="$otdir"
    ninja

if [ $? -ne '0' ]; then
    echo "ERROR: OPENTRACK BUILD FAILED"
    exit 1
fi

ninja install

if [[ -n $onnxruntime_dir ]]; then
    cp $otsrc/build/onnxruntime-src/lib/libonnxruntime.so.1.6.0 $otdir/libexec/opentrack/
fi
# make install

# Add desktop menu
echo "[Desktop Entry]
Comment[es_ES]=Seguimiento de los movimientos de la cabeza
Comment=
Exec="${otdir}/bin/opentrack"
Icon=input-gaming
MimeType=
Name[es_ES]=Opentrack
Name=Opentrack
Path=
Type=Application
Categories=Game;" | tee $desktop_dir/opentrack.desktop

ln -fs "${otdir}/bin/opentrack" "$HOME/.local/bin/"

pause
