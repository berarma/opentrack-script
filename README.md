# Opentrack script

Porgrama para compilar Opentrack en Linux

## Obtener Opentrack

Las diferentes opciones para obtener el codigo de Opentrack son:

- Descargar la ultima versión estable desde opentrack y descomprimela: https://github.com/opentrack/opentrack/releases
- Clonar opentrack desde git: `git clone https://github.com/opentrack/opentrack.git` 
- Clonar el fork con mejoras en wine: `git clone https://github.com/JT8D-17/opentrack.git`

El código de Opentrack se encontrará en una carpeta llamada opentrack/

## Añade el script:

Descarga el script de este repositorio:

https://gitlab.com/jugandoenlinux/opentrack-script/-/blob/main/z_Build.sh

Y copialo (o sobrescribelo) dentro del directorio de opentrack con la fuente.

## Instala los paquetes para compilar:

Instalas los siguientes paquetes en una consola.

Comando para ubuntu/debian:

`sudo apt install build-essential cmake cmake-curses-gui ninja-build qttools5-dev qtbase5-private-dev libprocps-dev libopencv-dev libevdev-dev gcc-multilib g++-multilib wine wine32 wine32-tools wine32-tools libqt5serialport5-dev qv4l2`

Comando para Arch/Manjaro:

TODO: `sudo `

## Ejecuta el script para compilar:

Dentro de directorio de la fuente ejecuta el script añadiendo los parametros opcionales:

`./z_Build.sh [parametro1] [parametro2] ...`

Parametros opcionales:
- `xplane` Compila con el plugin xplane.
- `onnx` Compila con el runtime ONNX para Neuralnet tracker.
- `aruco` Compila con el tracker [aruco](https://github.com/opentrack/opentrack/wiki/Aruco-tracker).
- `clean` Limpia el directorio build si anteriormente ya se hizo otra compilación.

El programa se instalará en `~/.opentrack-install` y creará una entrada en el menu de aplicaciones: `Juegos \ Opentrack`

## Plugin Xplane:

Copia el archivo `opentrack.xpl` desde `opentrack-install/libexec/opentrack` al directorio de `X-Plane 11/Resources/plugins`.

## Casos que se pueden dar:

- Opentrack no detecta ningún proton:
opentrack busca los perfiles de proton en la ruta ~/.local/share/Steam ([esto parece que no ocurre en el fork](https://github.com/JT8D-17/opentrack#2-automatic-detection-of-steam-apps-using-proton))
Si tu cliente de Steam se instala en otra ruta, haz un enlace simmbolico a la carpeta.
Por ejemplo en el caso de debian/ubuntu *quiźas* sea necesario con este comando:
`ln -s ~/.steam/debian-installation ~/.local/share/Steam`


- Casos que se pueden dar con wine:
[Consulta el repositorio del fork de Opentrack para temas relacionados con wine](https://github.com/JT8D-17/opentrack#wineproton-version-information)

***

## Créditos

Autor original del script *JT8D-17* en https://github.com/JT8D-17/

con licencia https://github.com/JT8D-17/opentrack#license-and-warranty

Y modificado por Francisco T. https://gitlab.com/franciscot (a.k.a. P_Vader) desde jugandoenlinux.com

***

## Garantía:
NO HAY GARANTÍA PARA ESTE PROGRAMA. TODO EL RIESGO EN RELACIÓN CON LA CALIDAD Y EL RENDIMIENTO DEL PROGRAMA
ES CON EL USUARIO. SI EL PROGRAMA RESULTA DEFECTUOSO, USTED ASUME EL COSTO DE
TODO EL SERVICIO, REPARACIÓN O CORRECCIÓN NECESARIOS.

